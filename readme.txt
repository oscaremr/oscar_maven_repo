-------
Purpose
-------
This project is just a maven repository.

This is where the Oscar and MyOscar and other related projects will publish our own jar files.

There's 2 uses for this project 
1) to allow offline working, i.e. a file system based local_repository
2) when jar's are committed into this repository, they can be reviewed then published online for everyone else (whom doesn't want offline working)


-------------------------------
Instructions for including Jars
------------------------------- 
We should only include jar files in here which are not otherwise available on any other online repositories.

Please use real version numbers and not SNAPSHOT versions, SNAPSHOT versions cause problems on our jenkins build system.

When installing jar's please make sure you include dependencies. i.e. 
mvn install:install-file -Dpackaging=jar -DcreateChecksum=true -DlocalRepositoryPath=./local_repo -DlocalRepositoryId=local_repo -DgroupId=org.oscarehr -DartifactId=example_project -Dversion=2013.05.09 -Dfile=example_project-2013.05.09.jar -DpomFile=example_project-2013.05.09.pom

In each project root please write a very brief readme.txt on where the jar came from and how to generate the jar file. Generally speaking this will fall into the categories of :
a) this is a oscar project XXX, the source is located at YYYY
b) this is generated classes from webservices or xml schema, the web service wsdl is located at XXX and use the commands YYYY and ZZZZ to generate the jars.